<?php

// procedural
$connection = mysqli_connect("localhost","root","","db");
$query = "SELECT * FROM users";
$result = mysqli_query($connection,$query);
/*
...
*/
mysqli_close($connection);

// obiectual
Class Database {

	private $connection;

	public function __construct(){
		$this->connection = mysqli_connect("localhost","root","","db");
	}

	public function query($query){
		return mysqli_query($this->connection,$query);
	}

	public function __destruct(){
		mysqli_close($this->connection);
	}

}

$database = new Database();
$database->query("SELECT * FROM users");