<?php
// http://php.net/manual/en/language.oop5.abstract.php
/*
Clasa Abstracta 
contine: proprietati, metode si declarare (abstracta) de metode
*/
Abstract Class FileSystem {

	protected $file;

	public function __construct($filename,$method){
		$this->file = fopen($filename,$method);
	}

	abstract public function do($line);

	public function __destruct(){
		fclose($this->file);
	}

}

Class FileSystemAppend extends FileSystem {

	public function __construct($filename){
		parent::__construct($filename,"a+");
	}

	public function do($line){
		fputs($this->file,$line.PHP_EOL);
	}

}

Class FileSystemWrite extends FileSystem {

	public function __construct($filename){
		parent::__construct($filename,"w");
	}

	public function do($line){
		fputs($this->file,$line.PHP_EOL);
	}

}

Class FileSystemRead extends FileSystem {

	public function __construct($filename){
		parent::__construct($filename,"r");
	}

	public function do($line){
		fgets($this->file);
	}

}

$object = new FileSystemWrite("file.txt");
$object->do("Ana has apples");