<?php

Class C1 {

	public $p1;

	public function m1(){

	}

	public static function m2(){
		self::m1(); // nu se poate apela in metoda statica o metoda non statica
	}

}

$object = new C1();
$object->m1();
$object->m2();

/*
o metoda statica se poate apela direct folosind numele clasei fara a instantia un obiect
*/
C1::m2();