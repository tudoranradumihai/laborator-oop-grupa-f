<?php
echo __FILE__."<br>";
/*
include "Controllers/C1.php";
include "Controllers/C2.php";
include "Models/C3.php";
*/
function __autoload($p1){
	echo __FUNCTION__."|".$p1."<br>";
	$folders = array("Controllers","Models");
	foreach($folders as $folder){
		$filepath = "$folder/$p1.php";
		if(file_exists($filepath)){
			require $filepath;
		}
	}
}

$o1 = new C1();
$o2 = new C2();
$o3 = new C3();
