<?php

// PROCEDURAL
$file = fopen("file.txt","a+");
fputs($file,"Ana has apples".PHP_EOL);
fclose($file);

// OBIECTUAL
Class FileSystem {

	private $file;

	public function __construct($filename,$method){
		$this->file = fopen($filename,$method);
	}

	public function write($line){
		fputs($this->file,$line.PHP_EOL);
	}

	public function read(){
		return fgets($this->file);
	}

	public function __destruct(){
		fclose($this->file);
	}

}

$object = new FileSystem("file.txt","a+");
$object->write("Ana has apples");
$object->read();