<?php

function __autoload($className){
	$folders = array("CLS1","CLS2");
	foreach($folders as $folder){
		$filePath = "$folder/$className.php";
		if(file_exists($filePath)){
			require $filePath;
		}
	}
}

$o1 = new C1(1,2);
C2::st1();
$o4 = new C4();

$o5 = new C5();
$o4->m1();
$o5->m2();