<?php

Class UsersController extends Controller {

	public function defaultAction(){
		
	}

	public function newAction(){
		require "Views/Users/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$validation = true;
			/* 
				HOMEWORK:
				1. required fields
				2. email type email
				3. password 8 characters
				4. password1 = password2
			*/
			if($validation){
				$user = new Users();
				$user->firstname = $_POST["firstname"];
				$user->lastname = $_POST["lastname"];
				$user->identifier = $_POST["identifier"];
				$user->birthdate = $_POST["birthdate"];
				$user->email = $_POST["email"];
				$user->password = md5($_POST["password"]);
				Users::insert($user);
			} else {
				/* Add errors in session */
				header("Location: index.php?C=Users&A=new");
			}
		}
		//require "Views/Users/create.php";
	}


}