<?php
require "Helpers/Autoload.php";
require "Helpers/Initialise.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<?php require "Resources/Partials/HeaderResources.php"; ?>
	</head>
	<body>
		<div class="container">
			<header>
				<?php require "Resources/Partials/Header.php"; ?>
			</header>
			<main>
				<?php initialise(); ?>
			</main>
			<footer>
				<?php require "Resources/Partials/Footer.php"; ?>
			</footer>
		</div>
		<?php require "Resources/Partials/FooterResources.php"; ?>
	</body>
</html>