<?php

Class Users {

	public $id;
	public $firstname;
	public $lastname;
	public $identifier;
	public $birthdate;
	public $email;
	public $password;

	public static function insert($user){
		$properties = array();
		$values = array();
		foreach($user as $property => $value){
			if($value!=""){
				array_push($properties,$property);
				array_push($values,"'".$value."'");
			}
		}
		$query = "INSERT INTO users (".implode(",",$properties).") VALUES (".implode(",",$values).");";
		$database = new Database();
		$database->query($query);
	}

}