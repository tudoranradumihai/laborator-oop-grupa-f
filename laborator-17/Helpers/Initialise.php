<?php

function initialise(){
	if(array_key_exists("C", $_GET)){
		$controller = ucfirst($_GET["C"])."Controller";
	} else {
		$controller = "DefaultController";
	}

	if(array_key_exists("A", $_GET)){
		$action = lcfirst($_GET["A"])."Action";
	} else {
		$action = "defaultAction";
	}

	if(class_exists($controller)){
		$object = new $controller();
		if(method_exists($object, $action)){
			$object->$action();
		} else {
			DefaultController::errorAction();
			//die("Method '$controller::$action' doesn't exist.");
		}
	} else {
		DefaultController::errorAction();
		//die("Class '$controller' doesn't exist.");
	}
}