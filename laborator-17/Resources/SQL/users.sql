CREATE TABLE users (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(255),
	lastname VARCHAR(255),
	identifier VARCHAR(255),
	birthdate VARCHAR(255),
	email VARCHAR(255),
	password VARCHAR(255)
);