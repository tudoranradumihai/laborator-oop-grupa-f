<!-- HOMEWORK: jquery validation -->
<h1>Register</h1>
<form id="users-new" method="POST" action="index.php?C=Users&A=create">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label for="firstname">Firstname</label>
			<input type="text" class="form-control" id="firstname" name="firstname" placeholder="John">
		</div>
		<div class="form-group col-md-6">
			<label for="lastname">Lastname</label>
			<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Doe">
		</div>
		<div class="form-group col-md-6">
			<label for="identifier">Identifier</label>
			<input type="text" class="form-control" id="identifier" name="identifier" placeholder="1000000123456">
		</div>
		<div class="form-group col-md-6">
			<label for="birthdate">Birth Date</label>
			<input type="date" class="form-control" id="birthdate" name="birthdate" placeholder="">
		</div>
		<div class="form-group col-md-12">
			<label for="email">Email</label>
			<input type="email" class="form-control" id="email" name="email" placeholder="john.doe@domain.com">
		</div>
		<div class="form-group col-md-6">
			<label for="password">Password</label>
			<input type="password" class="form-control" id="password" name="password">
		</div>
		<div class="form-group col-md-6">
			<label for="password2">Confirm Password</label>
			<input type="password" class="form-control" id="password2" name="password2"	>
		</div>
		<div class="form-group col-md-12 text-right">
			<button type="submit" class="btn btn-primary">Register</button>
		</div>
	</div>
</form>