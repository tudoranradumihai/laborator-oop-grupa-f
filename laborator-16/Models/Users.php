<?php

Class Users {

	public $id;
	public $firstname;
	public $lastname;
	public $email;

	public static function findAll(){
		$database = new Database();
		$query = "SELECT * FROM users";
		$result = $database->query($query);
		$array = array();
		while($line = mysqli_fetch_assoc($result)){
			$user = new Users();
			$user->id = $line["id"];
			$user->firstname = $line["firstname"];
			$user->lastname = $line["lastname"];
			$user->email = $line["email"];
			array_push($array,$user);
		}
		return $array;
	}

}