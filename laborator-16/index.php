<?php
// function __autoload()
function __autoload($class){
	// $folders contains the folder names where classes could exist
	$folders = array("Controllers","Models","Helpers");
	foreach($folders as $folder){
		// create path
		$path = "$folder/$class.php";
		// check if path exists
		if(file_exists($path)){
			// require the path
			require $path;
		}
	}
}
?>

<a href="index.php">Homepage</a> |
<a href="index.php?C=Users&A=new">New User</a> | 
<a href="index.php?C=Users&A=list">Users List</a> | 
<hr>
<?php

// FRONT CONTROLLER DESIGN PATTERN for MVC

// check if $_GET[C] exists
if(array_key_exists("C", $_GET)){
	// create $controller class name
	$controller = $_GET["C"]."Controller";
} else {
	$controller = "";
}
// check if $_GET[A] exists
if(array_key_exists("A", $_GET)){
	// create $action method name
	$action = $_GET["A"]."Action";
} else {
	$action = "";
}
// check if $controller class exists
if(class_exists($controller)){
	// create object from class
	$object = new $controller();
	// check if $action method exists
	if(method_exists($object, $action)){
		// call method
		$object->$action();
	} else {
		echo "ERROR: Method '$controller::$action' doesn't exists.";
	}
} else {
	echo "ERROR: Class '$controller' doesn't exists.";
}
?>