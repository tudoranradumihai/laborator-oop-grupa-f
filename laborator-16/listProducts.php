<?php

// V1 basic
$connection = mysqli_connect("","","","");
$query = "SELECT * FROM products";
$result = mysqli_query($connection,$query);
while($line = mysqli_fetch_assoc($result)){
	echo $line["id"];
}
mysqli_close($connection);

// V2 afisare cu bucla foreach
$connection = mysqli_connect("","","","");
$query = "SELECT * FROM products";
$result = mysqli_query($connection,$query);
$array = array();
while($line = mysqli_fetch_assoc($result)){
	array_push($array,$line);
}
foreach ($array as $line){
	echo $line["id"];
}
mysqli_close($connection);