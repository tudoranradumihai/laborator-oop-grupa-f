<?php

Class UsersController {

	public function newAction(){
		echo __METHOD__."<br>";
		require "Views/Users/new.php";
	}

	public function listAction(){
		$users = Users::findAll();
		require "Views/Users/list.php";
	}

}