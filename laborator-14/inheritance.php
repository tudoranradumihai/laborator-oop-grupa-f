<?php

Class C1 {

	public $p1;
	private $p3;

}

Class C2 extends C1 {

	public $p2;
	private $p4;

	public function setP3($p3){
		$this->p3 = $p3;
	}

	public function getP3(){
		return $this->p3;
	}

}

//$o1 = new C1();
//print_r($o1);
//$o1->setP3("3");
//echo $o1->getP3();

$o2 = new C2();
print_r($o2);
$o2->setP3("3");
echo $o2->getP3();


Class C1 {

	public $p1;
	protected $p2;
	private $p3;

	public function display(){
		echo $this->p1; // OK
		echo $this->p2; // OK
		echo $this->p3; // OK
	}

}

Class C2 extends C1 {

	public function display(){
		echo $this->p1; // OK
		echo $this->p2; // OK
		echo $this->p3; // ERROR
	}

}

$obj = new C1();
echo $obj->p1; // OK
echo $obj->p2; // ERROR
echo $obj->p3; // ERROR