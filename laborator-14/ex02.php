<!--
Vehicul - Autovehicul 	- Mercedes	- EClasse
									- CClasse
						- Audi 		- A4
									- A6
		- Motociclu		- Piaggio	- Scariletti
-->
<?php
Class Vehicul {
	public $roti;
	public $marca;
	public $capacitate;
}
Class Autovehicul extends Vehicul {
	public function __construct(){
		$this->roti = 4;
	}
}
Class Mercedes extends Autovehicul {
	public function __construct(){
		parent::__construct();
		$this->marca = "Mercedes";
	}
}
Class EClasse extends Mercedes {
	public function __construct($capacitate){
		parent::__construct();
		$this->capacitate = $capacitate;
	}
}
$car = new EClasse(2.2);
