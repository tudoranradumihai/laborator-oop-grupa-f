<?php

Class Vehicul {
	public $roti;	
	public $marca;
}

Class Autovehicul extends Vehicul {
	public function __construct(){
		$this->roti = 4;
	}
}

Class Audi extends Autovehicul {
	public function __construct(){
		parent::__construct();
		$this->marca = "AUDI";
	}
}

$obj = new Audi();
var_dump($obj);