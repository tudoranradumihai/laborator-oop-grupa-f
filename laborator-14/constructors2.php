<?php

Class C1 {

	private $p1;
	private $p2;

	public function __construct($p1,$p2){
		$this->p1 = $p1;
		$this->p2 = $p2;
	}

	public function m1(){
		echo "M1<br>";
	}

}

$obj = new C1(1,2);

var_dump($obj);