<?php

Class C1 {

	public function __construct(){
		echo __METHOD__;
	}

}

Class C2 extends C1 {

	public function __construct(){
		parent::__construct();
		echo __METHOD__;
	}

}

$o = new C2();