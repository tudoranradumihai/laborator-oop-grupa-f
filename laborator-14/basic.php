<?php

// 1.http://php.net/manual/en/language.oop5.php
// 2.http://php.net/manual/en/language.oop5.basic.php
// 3.http://php.net/manual/en/language.oop5.properties.php
// 4.http://php.net/manual/en/language.oop5.constants.php
// 5.http://php.net/manual/en/language.oop5.visibility.php

// CLASS
Class Users {
	// DECLARE CONSTANTS
	const MYCONST = "MYCONST";
	// DECLARE PROPERTIES
	// visibility (public|protected|private) $property (+ OPTIONAL) = "STRING";
	public $id;
	private $name;
	public $email = "...@..."; // default property set
	// DECLARE METHODS
	public function displayHelloWorld(){
		echo "Hello World!!!<br>";
	}
	public function getId(){
		// how to use a property inside the class
		return $this->id;
	}
	public function getName(){
		return $this->name;
	}
	public function setName($name){
		$this->name = $name;
	}
	public function displayName(){
		// how to call a method inside another method
		echo self::getName();
	}
	public function displayConst(){
		// how to call a const inside a method
		echo self::MYCONST;
	}
	public static function staticMethod(){
		echo "Static Method";
	}
}
// OBJECT
// object is a class instance
$user1 = new Users();
// set property value
$user1->id = 1;
// get property value
echo $user1->id."<br>";
// call method
$user1->displayHelloWorld();
// get constant value from object
echo $user1::MYCONST."<br>";
// get constant value from Class
echo Users::MYCONST."<br>";
// set private property value
$user1->setName("Radu");
// get private property value
echo $user1->getName();
// call static method from object
$user1->staticMethod();
// call static method from class
Users::staticMethod();